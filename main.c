#include <stdio.h>
#include <stdlib.h>
unsigned long p = 29;

int main(int argc, char *argv[])
{
    unsigned long n = atoi(argv[1]);
    unsigned long count = 0;
    while (n % p == 0)
    {
        count++;
        n /= p;
    }
    printf("%d,%d,C,This is not portable\n");
    return 0;
}
